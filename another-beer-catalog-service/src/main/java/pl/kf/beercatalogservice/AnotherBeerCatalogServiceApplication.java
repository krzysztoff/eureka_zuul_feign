package pl.kf.beercatalogservice;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.springframework.stereotype.Component;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.stream.Stream;

@SpringBootApplication
@EnableEurekaClient
public class AnotherBeerCatalogServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(AnotherBeerCatalogServiceApplication.class, args);
	}
}
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
class Beer {

	public Beer(String name) {
		this.name = name;
	}

	@Id
	@GeneratedValue
	private Long id;

	private String name;
}

@RepositoryRestResource
interface BeerRepository extends JpaRepository<Beer, Long> {}

@Component
class BeerInitializer implements CommandLineRunner {

	private final BeerRepository beerRepository;

	BeerInitializer(BeerRepository beerRepository) {
		this.beerRepository = beerRepository;
	}

	@Override
	public void run(String... args) throws Exception {
		Stream.of("AAA","BBB")
//        Stream.of("AAA")
				.forEach(beer -> beerRepository.save(new Beer(beer)));

		beerRepository.findAll().forEach(System.out::println);
	}
}